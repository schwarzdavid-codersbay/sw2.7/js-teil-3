const myArray = [1, 6, 8, 3, 8, 4, 6, 2]
// 1 === 1

myArray.sort()

const personX = {
    firstName: 'Jonas',
    age: 45
}
const myArray2 = [
    {
        firstName: 'Gustav',
        age: 17
    },
    {
        firstName: 'John',
        age: 25
    },
    {
        firstName: 'Johannes',
        age: 15
    },
    {
        firstName: 'Henry',
        age: 55
    },
    {
        firstName: 'Frederike',
        age: 12
    },
    personX
]
// {} ==== {} -> false

// const x = {}
// const y = x
// x === y -> true

myArray2
    .sort((a, z) => a.firstName.toLowerCase().localeCompare(z.firstName.toLowerCase()))
    .filter(person => person.age > 18)
    .find(person => person.firstName === 'Herbert')

const indexOfJonas = myArray2.indexOf(personX)
const indexOfFrederike = myArray2.indexOf({firstName: 'Frederike'})

const indexOfHenry = myArray2.findIndex(person => person.firstName === 'Henry')
console.log(indexOfHenry)
console.log(myArray2[indexOfHenry])

if (indexOfHenry >= 0) {
    console.trace('Henry gefunden')
}

const henry = myArray2.find(person => person.firstName === 'Henry')
console.log(henry)

myArray2
    .filter(person => person.age >= 18)
    .map(person => person.firstName.toUpperCase())
    .forEach(firstName => {
        const btn = document.createElement('button')
        btn.textContent = firstName
        btn.addEventListener('click', e => {
            alert(firstName + ' clicked')
            e.stopPropagation()
        })
        btn.addEventListener('click', function () {
            console.log(this)
        })
        document.body.appendChild(btn)
    })

const ageSum = myArray2.reduce((output, person) => {
    console.log(output, person)
    return output + person.age
}, 0)
console.log(ageSum / myArray2.length)

const isEveryPersonOver18 = myArray2.every(person => person.age >= 18)
console.log(isEveryPersonOver18)

const isAnyPersonOver18 = myArray2.some(person => person.age >= 18)
console.log(isAnyPersonOver18)

/*const a = document.createElement('a')
a.href = 'arrays.js'
a.download = 'Arrays.js'
a.click()*/



myFn2()
//myFn()
//myFn3()

const myFn = () => {
    console.log("Hallo Welt")
}

const myFn3 = function () {
    console.log("Hallo Welt")
}

function myFn2() {
    console.log("Hallo Welt")
}


/*const books = [
    {
        author: 'Christian',
        name: 'Whatever'
    },
    {
        author: 'Christian',
        name: 'Dings'
    },
    {
        author: 'Kyra',
        name: 'Foo'
    },
    {
        author: 'Kyra',
        name: 'Bar'
    }
]

const authors = Array.from(
    books.reduce((output, book) => {
        output.add(book.author)
        return output
    }, new Set())
)
console.log(authors)*/


const a = {
    b: {
        c: {
            d: {
                e: 'Hello World'
            }
        }
    }
}

if(a.b && a.b.c && a.b.c.d) {
    console.log(a.b.c.d.e)
} else {
    console.log('Standardwert')
}

console.log('' || 'Standardwert') // 'Standardwert'
console.log('' ?? 'Standardwert') // ''
console.log(a.b?.c?.d?.e ?? 'Standardwert')

const button = document.getElementById('light-dark-switch')
if(button) {
    button.addEventListener('click', () => console.log('Dings'))
}

document.getElementById('light-dark-switch')?.addEventListener('click', () => console.log('Dings'))

const age = 19
const y = age >= 18 ? 'Erwachsen' : 'Kind'
console.log(y)




const firstNameInput = document.getElementById('first-name-input');
const lastNameInput = document.getElementById('last-name-input');
const birthdateInput = document.getElementById('birthdate-input');

const persons = []

function addPerson() {
    const firstName = firstNameInput?.value ?? 'Max'
    const lastName = lastNameInput?.value ?? 'Muster'
    const birthdate = birthdateInput?.value ?? '01-01-2023'

    persons.push({
        firstName,
        lastName,
        birthdate,
        save() {
            console.log(this)
            console.log(this.firstName, this.lastName, this.birthdate)
        }
    })
}

addPerson()
persons.forEach(person => person.save())

document.addEventListener('DOMContentLoaded', function () {
    console.log(this)
})

const circle = document.getElementById('circle')
document.addEventListener('pointermove', e => {
    circle.style.top = e.clientY + 'px'
    circle.style.left = e.clientX + 'px'
})

document.body.addEventListener('click', () => {
    console.log('Click')
})